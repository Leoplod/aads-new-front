import React from 'react'
import Modal from 'react-modal'
import { connect } from 'react-redux'
import css from './style.scss'

import DepositFunds from 'account/DepositFunds/DepositFunds'
import WithdrawFunds from 'account/WithdrawFunds/WithdrawFunds'

const MODAL_COMPONENTS = {
  'DEPOSIT_FUNDS': DepositFunds,
  'WITHDRAW_FUNDS': WithdrawFunds
}

const ModalRoot = ({ modalType, modalProps, dispatch }) => {
  if (!modalType) {
    return null
  }

  const SpecificModal = MODAL_COMPONENTS[modalType]
  return (
    <Modal
      isOpen={true}
      overlayClassName={css.overlay}
      className={css.content}
    >
      <SpecificModal {...modalProps} />
    </Modal>
  )
}

export default connect(
  state => state.modalState
)(ModalRoot)
