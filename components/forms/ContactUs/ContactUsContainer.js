import React from 'react'
import ContactUs from './ContactUs'
import Form from './Form'
import { withFormik } from 'formik'
import * as Yup from 'yup'

export default class extends React.Component {
  constructor(props) {
    super(props)
  }

  renderForm() {
    const FormEnhanced = withFormik({
      mapPropsToValues: () => ({
        name: '',
        email: '',
        title: null,
        text: '',
        privacy_policy: 0,
        captcha: ''
      }),
      validationSchema: Yup.object().shape({
        name: Yup.string()
          .required('Обязательное поле'),
        email: Yup.string()
          .email('Некорректный email')
          .required('Обязательное поле'),
        title: Yup.mixed()
          .notOneOf([null], 'Обязательное поле'),
        text: Yup.string()
          .required('Обязательное поле'),
        captcha: Yup.string()
          .required('Обязательное поле'),
        privacy_policy: Yup.mixed()
          .oneOf([1], 'Обязательное поле'),
      }),
      handleSubmit: this.handleSubmit
    })(Form)

    return <FormEnhanced />
  }

  render() {
    return <ContactUs
      form={this.renderForm()}
    />
  }

  handleSubmit(values) {
    console.log(values)
  }
}
