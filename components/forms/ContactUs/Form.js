import React from 'react'
import PropTypes from 'prop-types'
import { Input, Select, Textarea, Checkbox, Button, Captcha } from 'ui'

export default Form

Form.propTypes = {
  values: PropTypes.object,
  handleChange: PropTypes.func,
  handleSubmit: PropTypes.func,
  setFieldValue: PropTypes.func,
  touched: PropTypes.object,
  errors: PropTypes.object
}

function Form({
  values,
  handleSubmit,
  handleChange,
  setFieldValue,
  touched,
  errors
}) {
  return (
    <form onSubmit={handleSubmit}>
      <div className="form-field">
        <Input
          id="input-1"
          type="text"
          name="name"
          value={values.name}
          onChange={handleChange}
          placeholder="Ваше имя"
          error={touched.name ? errors.name : ''}
        />
      </div>
      <div className="form-field">
        <Input
          id="input-2"
          type="text"
          name="email"
          value={values.email}
          onChange={handleChange}
          placeholder="Электронная почта"
          error={touched.email ? errors.email : ''}
        />
      </div>
      <div className="form-field">
        <Select
          placeholder="Выберите тему"
          options={[
            {value: 'subject1', label: 'Тема 1'},
            {value: 'subject2', label: 'Тема 2'},
            {value: 'subject3', label: 'Тема 3'},
          ]}
          onChange={selected => {
            setFieldValue('title', selected.value)
          }}
          error={touched.title ? errors.title : ''}
        />
      </div>
      <div className="form-field">
        <Textarea
          id="textarea-1"
          placeholder="Сообщение"
          name="text"
          value={values.text}
          onChange={handleChange}
          error={touched.text ? errors.text : ''}
        />
      </div>
      <div className="form-field">
        <Captcha
          onChange={value => setFieldValue('captcha', value)}
          error={touched.captcha ? errors.captcha : ''}
        />
      </div>
      <div className="form-field">
        <Checkbox
          id="checkbox-1"
          text={`Я соглашаюсь с <a href="#" target="_blank">политикой конфиденциальности</a> A-ADS`}
          name="privacy_policy"
          error={touched.privacy_policy ? errors.privacy_policy : ''}
          onChange={e => {
            setFieldValue('privacy_policy', +e.target.checked)
          }}
        />
      </div>
      <div className="form-submit">
        <Button type="submit">Связаться</Button>
      </div>
    </form>
  )
}
