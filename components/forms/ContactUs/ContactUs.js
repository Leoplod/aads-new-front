import React from 'react'
import 'forms/style.scss'
import PropTypes from 'prop-types'

export default ContactUs

ContactUs.propTypes = {
  form: PropTypes.element
}

function ContactUs({form}) {
  return (
    <div className='form-wrapper'>
      <div className='form-inner'>
        <h1 className='form-title'>Связаться с нами</h1>
        <p className='form-subtitle'>
          По общим вопросам, включая возможности партнёрства,
          запросы маркетинга и пиара, пожалуйста, заполните эту
          форму ниже.
        </p>
        {form}
      </div>
    </div>
  )
}
