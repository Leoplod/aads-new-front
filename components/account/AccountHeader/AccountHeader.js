import React from 'react'
import css from './style.scss'
import LogoImgSrc from './img/logo.svg'
import Balance from './components/Balance/Balance'
import Notifications from './components/Notifications/Notifications'
import Help from './components/Help/Help'
import UserMenu from './components/UserMenu/UserMenu'

function AccountHeader() {
  return (
    <div className={css.header}>
      <a href="#" className={css.logo}><LogoImgSrc /></a>

      <div className={css.controls}>
        <div className={css.balanceWrap}>
          <Balance />
        </div>
        <div className={css.notificationsWrap}>
          <Notifications />
        </div>
        <div className={css.helpWrap}>
          <Help />
        </div>
        <div className={css.userMenuWrap}>
          <UserMenu />
        </div>
      </div>
    </div>
  )
}

export default AccountHeader
