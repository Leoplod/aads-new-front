import React, { useState, useRef, useEffect } from 'react'
import css from './style.scss'
import cn from 'classnames'
import HelpImgSrc from './img/help.svg'

export default function () {
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const refWrapper = useRef(null)

  function handleClickOutside(e) {
    if (refWrapper && !refWrapper.current.contains(e.target)) {
      setIsMenuOpen(false)
    }
  }

  useEffect(() => {
    document.addEventListener('click', handleClickOutside)

    return () => {
      document.removeEventListener('click', handleClickOutside)
    }
  }, [])

  return (
    <div className={css.help} ref={refWrapper}>
      <div className={css.helpIco} onClick={e => setIsMenuOpen(true)}><HelpImgSrc /></div>

      {isMenuOpen &&
        <div className={cn('dropdown-menu', css.menu)}>
          <a href="#" className={'dropdown-menu__item active'}>База знаний</a>
          <a href="#" className={'dropdown-menu__item'}>Блог</a>
          <a href="#" className={'dropdown-menu__item'}>Техподдержка</a>
        </div>
      }
    </div>
  )
}
