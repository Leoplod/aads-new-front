import React, { useState, useRef, useEffect } from 'react'
import css from './style.scss'
import cn from 'classnames'
import VImgSrc from './img/v.svg'

export default function () {
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const refWrapper = useRef(null)

  function handleClickOutside(e) {
    if (refWrapper && !refWrapper.current.contains(e.target)) {
      setIsMenuOpen(false)
    }
  }

  useEffect(() => {
    document.addEventListener('click', handleClickOutside)

    return () => {
      document.removeEventListener('click', handleClickOutside)
    }
  }, [])

  return (
    <div className={css.userMenu} ref={refWrapper}>
      <div className={css.userName} onClick={e => setIsMenuOpen(true)}>username <span><VImgSrc/></span></div>
      {isMenuOpen &&
        <div className={cn('dropdown-menu', css.menu)}>
          <a href="#" className={'dropdown-menu__item active'}>Settings</a>
          <a href="#" className={'dropdown-menu__item'}>Logout</a>
        </div>
      }
    </div>
  )
}
