import React, { useState, useEffect, useRef } from 'react'
import css from './style.scss'
import BellImgSrc from './img/bell.svg'
import NotificationsList from './NotificationsList'

export default function () {
  const [isNotificationsOpen, setIsNotificationsOpen] = useState(false)
  const refWrapper = useRef(null)

  function handleClickOutside(e) {
    if (refWrapper && !refWrapper.current.contains(e.target)) {
      setIsNotificationsOpen(false)
    }
  }

  useEffect(() => {
    document.addEventListener('click', handleClickOutside)

    return () => {
      document.removeEventListener('click', handleClickOutside)
    }
  }, [])

  return (
    <div className={css.notifications} ref={refWrapper}>
      <div className={css.bell} onClick={e => {setIsNotificationsOpen(true)}}><BellImgSrc /></div>
      {isNotificationsOpen &&
        <NotificationsList onClickClose={e => setIsNotificationsOpen(false)} css={css} />
      }
    </div>
  )
}
