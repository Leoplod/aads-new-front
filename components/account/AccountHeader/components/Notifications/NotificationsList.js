import React from 'react'
import cn from 'classnames'

export default function ({
  css,
  onClickClose
}) {
  return (
    <div className={css.list}>
      <p className={cn('h3-like', css.listTitle)}>Notifications</p>
      <div className={css.listClose} onClick={onClickClose}></div>
      <p className={cn('h4-like', css.listAll)}><a href="#">All notifications</a></p>

      <div className={css.itemBlock}>
        <p className={css.itemBlockTitle}>Unread&nbsp;&nbsp; 2</p>

        <div className={css.item}>
          <p className={css.itemText}>
            На счете рекламной кампании #1 недостаточно средств для оплаты наград.
            Пожалуйста, пополните счет, чтобы обеспечить непрерывность вашей
            рекламной кампании.
          </p>
          <p className={css.itemDate}><span>21.10.2019</span></p>
        </div>

        <div className={css.item}>
          <p className={css.itemText}>
            На счете рекламной кампании #1 недостаточно средств для оплаты наград.
            Пожалуйста, пополните счет, чтобы обеспечить непрерывность вашей
            рекламной кампании.
          </p>
          <p className={css.itemDate}><span>21.10.2019</span></p>
        </div>

      </div>

      <div className={css.itemBlock}>
        <p className={css.itemBlockTitle}>Recent</p>

        <div className={css.item}>
          <p className={css.itemText}>
            На счете рекламной кампании #1 недостаточно средств для оплаты наград.
            Пожалуйста, пополните счет, чтобы обеспечить непрерывность вашей
            рекламной кампании.
          </p>
          <p className={css.itemDate}><span>21.10.2019</span></p>
        </div>

        <div className={css.item}>
          <p className={css.itemText}>
            На счете рекламной кампании #1 недостаточно средств для оплаты наград.
            Пожалуйста, пополните счет, чтобы обеспечить непрерывность вашей
            рекламной кампании.
          </p>
          <p className={css.itemDate}><span>21.10.2019</span></p>
        </div>
      </div>

      <p className={css.listShowMore}><span className='a-like'>Show More</span></p>
    </div>
  )
}
