import React, { useEffect, useState, useRef } from 'react'
import css from './style.scss'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import DepositImgSrc from './img/money.svg'
import VImgSrc from './img/v.svg'
import cn from 'classnames'
import * as modalActions from 'store/modules/modal/actions'

const Balance = ({ showModal }) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const refWrapper = useRef(null)

  function handleClickOutside(e) {
    if (refWrapper && !refWrapper.current.contains(e.target)) {
      setIsMenuOpen(false)
    }
  }

  useEffect(() => {
    document.addEventListener('click', handleClickOutside)

    return () => {
      document.removeEventListener('click', handleClickOutside)
    }
  }, [])

  return (
    <div className={css.balance} ref={refWrapper}>
      <div className={css.balanceInner} onClick={e => setIsMenuOpen(true)}>
        <div className={css.sum}>0.00000232</div>
        <div className={css.currency}>BTC</div>
        <div className={css.v}><VImgSrc /></div>
      </div>
      <div
        className={css.depositButton}
        onClick={() => showModal('DEPOSIT_FUNDS')}
      >
        <DepositImgSrc />
      </div>

      {isMenuOpen &&
        <div className={cn('dropdown-menu', css.menu)}>
          <a href="#" onClick={e => showModal('DEPOSIT_FUNDS')} className={'dropdown-menu__item'}>Deposit</a>
          <a href="#" onClick={e => showModal('WITHDRAW_FUNDS')} className={'dropdown-menu__item'}>Withdraw</a>
          <a href="#" className={'dropdown-menu__item'}>Finance Page</a>
        </div>
      }
    </div>
  )
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(modalActions, dispatch)
}

export default connect(null, mapDispatchToProps)(Balance)
