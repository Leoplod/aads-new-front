import React from 'react'
import css from './style.scss'
import cn from 'classnames'

import BurgerImgSrc from './img/burger.svg'
import InfoImgSrc from './img/info.svg'
import CampaignsImgSrc from './img/campaigns.svg'
import AdUnitsImgSrc from './img/ad_units.svg'
import FinancesImgSrc from './img/finances.svg'

const menuItems = [
  {
    name: 'Info',
    link: '#'
  },
  {
    name: 'Finance',
    link: '#',
    children: [
      {
        name: 'Stats',
        link: '#'
      },
      {
        name: 'Withdrawals',
        link: '#'
      },
      {
        name: 'Deposits',
        link: '#'
      },
      {
        name: 'Withdrawals Options',
        link: '#'
      }
    ]
  },
  {
    name: 'Campaigns',
    link: '#',
    count: 10
  },
  {
    name: 'Ad Units',
    link: '#',
    count: 0
  },
  {
    name: 'Events',
    link: '#',
    count: 2
  }
]

function AccountSidebar() {
  return (
    <div className={css.menuWrap}>
      <div className={css.burger}>
        <BurgerImgSrc />
      </div>

      <div className={css.menu}>
        <a href="#" className={cn(css.menuItem, css.active)}>
          <InfoImgSrc />
        </a>

        <a href="#" className={css.menuItem}>
          <CampaignsImgSrc />
        </a>

        <a href="#" className={css.menuItem}>
          <AdUnitsImgSrc />
        </a>

        <a href="#" className={css.menuItem}>
          <FinancesImgSrc />
        </a>
      </div>
    </div>
  )
}

export default AccountSidebar
