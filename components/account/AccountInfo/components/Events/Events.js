import React from 'react'
import css from './style.scss'
import { Button } from 'ui'

function Events({
  items
}) {
  return (
    <div className={css.events}>
      <div className={css.head}>
        <h3 className={css.title}>Events</h3>
      </div>
      <div className={css.body}>
        {items.map((item, index) => (
          <div key={index} className={css.item}>
            <pre dangerouslySetInnerHTML={{__html: item.value.trim()}} />
          </div>
        ))}
      </div>
      <div className={css.seeAll}><Button blue>See all Events</Button></div>
    </div>
  )
}

export default Events
