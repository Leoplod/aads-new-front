import React from 'react'
import CampaignsControlBlock from 'account/Campaigns/CampaignsControlBlock'
import css from './style.scss'

function AccountInfo() {
  return (
    <div className={css.info}>
      <CampaignsControlBlock />
    </div>
  )
}

export default AccountInfo
