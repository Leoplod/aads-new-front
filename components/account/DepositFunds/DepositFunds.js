import React from 'react'
import { Select, QRCode } from 'ui'
import * as modalActions from 'store/modules/modal/actions'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import cn from 'classnames'
import css from './style.scss'

const DepositFunds = ({ closeModal }) => {
  const depositAddress = '1ATyQ6GjCzCoPh1mp52jiDdDAKnM9jbAfM'

  return (
    <div className={cn(css.depositFunds, 'popup')}>
      <div className='popup-close' onClick={closeModal} />
      <p className={cn(css.title, 'popup-title')}>Deposit Funds</p>
      <div className={cn('popup-body')}>
        <div className={css.depositAddress}>
          <p className={css.depositAddressTitle}>Адрес пополнения</p>
          <Select
            className={css.depositAddressSelect}
            options={[{
              label: 'Bitcoin (BTC)',
              value: 'btc'
            }]}
          />
          <p className={css.depositAddressText}><span>{depositAddress}</span></p>
          <p className={css.termsOfConditions}>Зачисляя средства на счет вы принимаете <a href="#">Условия использования</a></p>
          <div className={css.depositAddressQrCodeWrap}>
            <QRCode options={{width: 240, height: 240}} text={depositAddress} />
          </div>
        </div>

        <div className={css.solutions}>
          <p className='h4-like'><a href="#">How to buy cryptocurrency</a></p>
          <p className='h4-like'><a href="#">How to make a deposit to A-ADS balance</a></p>
        </div>
      </div>
    </div>
  )
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(modalActions, dispatch)
}

export default connect(null, mapDispatchToProps)(DepositFunds)
