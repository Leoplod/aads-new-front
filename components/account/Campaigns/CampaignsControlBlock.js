import React from 'react'
import { Button, SpecialButton } from 'ui'
import cn from 'classnames'
import css from './style.scss'

function CampaignsControlBlock() {
  return (
    <div className={cn('units-control', css.campaignsBlock)}>
      <div className='units-control__header'>
        <p className={'units-control__title'}>Campaigns</p>
        <div className={css.createCampaignButton}><Button stretch={true}>Create New Campaign</Button></div>
      </div>

      <p className='units-control__small-title'>Today Overview</p>
      <div className={'units-control__stat'}>
        <div className={'units-control__stat-item'}>
          <p>6</p>
          <p>Active Campaigns</p>
        </div>
        <div className={'units-control__stat-item'}>
          <p>414</p>
          <p>Clicks Today</p>
        </div>
        <div className={'units-control__stat-item'}>
          <p>5413</p>
          <p>Impressions Today</p>
        </div>
        <div className={'units-control__stat-item'}>
          <p>0.0002321</p>
          <p>Spendings Today</p>
        </div>
      </div>

      <div className={'units-control__body'}>
        <div className={'units-table-wrap'}>
          <div className='units-table-wrap__header'>
            <p className='units-control__small-title'>Last Campaigns</p>
            <p><a href='#' className='h4-like'><strong>Go to the Campaign Page</strong></a></p>
          </div>

          <table className={'units-table'}>
            <thead>
              <tr>
                <th className={'--sortable'}>
                  Campaign Name
                  <span className={'units-table__arrow-down'}></span>
                  <span className={'units-table__arrow-up'}></span>
                </th>
                <th className={'--sortable'}>
                  Payment
                  <span className={'units-table__arrow-down'}></span>
                  <span className={'units-table__arrow-up'}></span>
                </th>
                <th className={'--sortable'}>
                  Impressions
                  <span className={'units-table__arrow-down'}></span>
                  <span className={'units-table__arrow-up'}></span>
                </th>
                <th className={'--sortable'}>
                  Clicks
                  <span className={'units-table__arrow-down'}></span>
                  <span className={'units-table__arrow-up'}></span>
                </th>
                <th className={'--sortable'}>CTR
                <span className={'units-table__arrow-down'}></span>
                  <span className={'units-table__arrow-up'}></span>
                </th>
                <th className={'--sortable'}>Spent, B
                <span className={'units-table__arrow-down'}></span>
                  <span className={'units-table__arrow-up'}></span>
                </th>
                <th className={cn('units-table__item-status-col', '--sortable', css.statusCol)}>Status
                <span className={'units-table__arrow-down'}></span>
                  <span className={'units-table__arrow-up'}></span>
                </th>
                <th className="units-table__item-settings-col"></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className={css.mainCol} rowSpan="2">
                  <p className={'units-table__item-title'}>Campaign #001</p>
                  <p className={'units-table__item-descr'}>The success of this type of advertising lies in the ability of the ad</p>
                  <p className={'units-table__item-url'}><a href='#'>example.com</a></p>
                </td>
                <td height="1%"><p>CPD</p></td>
                <td><p>166 320</p></td>
                <td><p>12 002</p></td>
                <td><p>7,2%</p></td>
                <td><p>0.00090000</p></td>
                <td className="units-table__item-status-col">
                  <p><span className={'units-table__item-status --green'}>Active</span></p>
                </td>
                <td className="units-table__item-settings-col" width="1%">
                  <div>
                    <SpecialButton buttonType="pause" />
                    <SpecialButton buttonType="settings" />
                    <SpecialButton buttonType="trash" />
                  </div>
                </td>
              </tr>
              <tr>
                <td></td>
              </tr>
              <tr><td className="units-table__item-line" colSpan="8" /></tr>

              <tr>
                <td className={css.mainCol} rowSpan="2">
                  <p className={'units-table__item-title'}>Campaign #003</p>
                  <p className={'units-table__item-descr'}>The success of this type of advertising lies in the ability of the ad</p>
                  <p className={'units-table__item-url'}><a href='#'>example.com</a></p>
                </td>
                <td height="1%"><p>CPD</p></td>
                <td><p>166 320</p></td>
                <td><p>12 002</p></td>
                <td><p>7,2%</p></td>
                <td><p>0.00090000</p></td>
                <td className="units-table__item-status-col">
                  <p><span className={'units-table__item-status --gray'}>Paused</span></p>
                </td>
                <td className="units-table__item-settings-col">
                  <div>
                    <SpecialButton buttonType="start" />
                    <SpecialButton buttonType="settings" />
                    <SpecialButton buttonType="trash" />
                  </div>
                </td>
              </tr>
              <tr>
                <td></td>
              </tr>
              <tr><td className="units-table__item-line" colSpan="8" /></tr>

              <tr>
                <td className={css.mainCol} rowSpan="2">
                  <p className={'units-table__item-title'}>Campaign #004</p>
                  <p className={'units-table__item-descr'}>The success of this type of advertising lies in the ability of the ad</p>
                  <p className={'units-table__item-url'}><a href='#'>example.com</a></p>
                </td>
                <td height="1%"><p>CPD</p></td>
                <td><p>166 320</p></td>
                <td><p>12 002</p></td>
                <td><p>7,2%</p></td>
                <td><p>0.00090000</p></td>
                <td className="units-table__item-status-col">
                  <p><span className={'units-table__item-status --green'}>Active</span></p>
                </td>
                <td className="units-table__item-settings-col">
                  <div>
                    <SpecialButton buttonType="pause" />
                    <SpecialButton buttonType="settings" />
                    <SpecialButton buttonType="trash" />
                  </div>
                </td>
              </tr>
              <tr>
                <td></td>
              </tr>
              <tr><td className="units-table__item-line" colSpan="8" /></tr>

              <tr>
                <td className={css.mainCol} rowSpan="2">
                  <p className={'units-table__item-title'}>Campaign #005</p>
                  <p className={'units-table__item-descr'}>The success of this type of advertising lies in the ability of the ad</p>
                  <p className={'units-table__item-url'}><a href='#'>example.com</a></p>
                </td>
                <td height="1%"><p>CPD</p></td>
                <td><p>166 320</p></td>
                <td><p>12 002</p></td>
                <td><p>7,2%</p></td>
                <td><p>0.00090000</p></td>
                <td className="units-table__item-status-col">
                  <p><span className={'units-table__item-status --gray'}>Paused</span></p>
                </td>
                <td className="units-table__item-settings-col">
                  <div>
                    <SpecialButton buttonType="start" />
                    <SpecialButton buttonType="settings" />
                    <SpecialButton buttonType="trash" />
                  </div>
                </td>
              </tr>
              <tr>
                <td></td>
              </tr>
              <tr><td className="units-table__item-line" colSpan="8" /></tr>
            </tbody>
          </table>

          <div className='units-table-wrap__footer'>
            <p className='center'><span onClick={e => {}} className='a-like'>Show all (12)</span></p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CampaignsControlBlock
