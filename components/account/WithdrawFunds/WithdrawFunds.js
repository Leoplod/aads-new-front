import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as modalActions from 'store/modules/modal/actions'
import { Button } from 'ui'
import cn from 'classnames'
import css from './style.scss'

const WithdrawFunds = ({ closeModal }) => {
  return (
    <div className={cn('popup', css.withdrawFunds)}>
      <div className='popup-close'
        onClick={closeModal}
      />
      <p className={cn(css.title, 'popup-title')}>Withdraw Funds</p>

      <div className={cn('popup-body', css.container)}>
        <p className={cn('h3-like', css.withdrawAddressSign)}>Withdrawal address</p>
        <p className={css.withdrawAddress}>3Jofwehhpowegkasiofe52</p>
        <Button stretch={true} className={css.withdrawAll}>Withdraw All Funds</Button>
        <p className={cn('h4-like', css.changeWithdrawAddress)}><span>Change Withdrawal Address</span></p>
      </div>
    </div>
  )
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(modalActions, dispatch)
}

export default connect(null, mapDispatchToProps)(WithdrawFunds)
