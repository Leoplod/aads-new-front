import React from 'react'
import PropTypes from 'prop-types'
import ReactSelect from 'react-select'
import { ValidationError } from 'ui/common'
import cn from 'classnames'
import css from './Select.scss'

export default Select

Select.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string
  })),
  onChange: PropTypes.func,
  isSearchable: PropTypes.bool,
  error: PropTypes.string,
  placeholder: PropTypes.string,
  className: PropTypes.string
}

function Select ({
  options,
  onChange,
  isSearchable = false,
  error = '',
  placeholder = '',
  className = ''
}) {
  return (
    <div className={cn(css.selectWrap, className)}>
      <ReactSelect
        className="select-container"
        classNamePrefix="select"
        options={options}
        isSearchable={isSearchable}
        placeholder={placeholder}
        onChange={onChange}
      />
      <div className={css.error}>
        <ValidationError error={error} />
      </div>
    </div>
  )
}
