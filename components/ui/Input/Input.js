import React from 'react'
import css from './Input.scss'
import PropTypes from 'prop-types'
import cx from 'classnames'
import { ValidationError } from 'ui/common'

export default Input

Input.propTypes = {
  type: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string,
  name: PropTypes.string,
  id: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  error: PropTypes.string
}

function Input({
  id,
  type = 'text',
  onChange,
  value = '',
  name = '',
  placeholder = '',
  error = ''
}) {
  return (
    <div
      className={cx(
        css.input,
        value.length && css.notEmpty
      )}
    >
      <div className={css.wrap}>
        <input
          name={name}
          id={id}
          value={value}
          onChange={onChange}
          type={type}
        />
        <label htmlFor={id}><span>{placeholder}</span></label>
      </div>
      <div className={css.error}>
        <ValidationError error={error} />
      </div>
    </div>
  )
}
