import React from 'react'
import PropTypes from 'prop-types'
import css from './Button.scss'
import cn from 'classnames'

export default Button

Button.propTypes = {
  onClick: PropTypes.func,
  type: PropTypes.string,
  children: PropTypes.node,
  blue: PropTypes.bool,
  stretch: PropTypes.bool,
}

function Button({
  onClick,
  type = null,
  children,
  stretch = false,
  blue = false,
  className = ''
}) {
  return (
    <button
      className={cn(
        className,
        css.button,
        stretch && css.stretch,
        blue && css.blue
      )}
      onClick={onClick}
      type={type}
    >
      {children}
    </button>
  )
}
