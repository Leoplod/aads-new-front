import React from 'react'
import PropTypes from 'prop-types'
import css from './ValidationError.scss'

export default ValidationError

ValidationError.propTypes = {
  error: PropTypes.string
}

function ValidationError({
  error = ''
}) {
  return (
    <React.Fragment>
      {error &&
        <div className={css.validationError}>
          <p>{error}</p>
        </div>
      }
    </React.Fragment>
  )
}
