import QRCodeLib from 'qrcode'
import { useRef, useEffect } from 'react'
import css from './style.scss'

export default function ({ text, options = {} }) {
  const canvasRef = useRef(null)

  useEffect(() => {
    QRCodeLib.toCanvas(canvasRef.current, text, {
      quality: options.quality || 0.4,
      margin: options.margin || 0,
      width: options.width || 200,
      height: options.height || 200,
      color: options.color || {
        dark: '#000',
        light: '#fff'
      }
    }, function (error) {
      if (error) console.error(error)
      console.log('success!');
    })
  }, [])

  return (
    <div className={css.qrCode}>
      <canvas ref={canvasRef}></canvas>
    </div>
  )
}
