import React from 'react'
import PropTypes from 'prop-types'
import css from './Checkbox.scss'
import CheckBoxInactiveImg from './img/checkbox-inactive.svg'
import CheckBoxActiveImg from './img/checkbox-active.svg'
import { ValidationError } from 'ui/common'

export default Checkbox

Checkbox.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string,
  text: PropTypes.string,
  value: PropTypes.string,
  defaultChecked: PropTypes.bool,
  onChange: PropTypes.func,
  error: PropTypes.string
}

function Checkbox({
  id,
  name,
  text,
  value,
  defaultChecked = false,
  onChange,
  error
}) {
  return (
    <div className={css.checkbox}>
      <div className={css.wrap}>
        <input
          type="checkbox"
          id={id}
          name={name}
          defaultChecked={defaultChecked}
          onChange={onChange}
        />
        <label htmlFor={id}>
          <div className={css.iconWrap}>
            <CheckBoxInactiveImg className={css.iconInactive} />
            <CheckBoxActiveImg  className={css.iconActive} />
          </div>
          <div className={css.text} dangerouslySetInnerHTML={{__html: text}} />
        </label>
      </div>
      <div className={css.error}>
        <ValidationError error={error} />
      </div>
    </div>
  )
}
