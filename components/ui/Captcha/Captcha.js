import React from 'react'
import PropTypes from 'prop-types'
import ReCAPTCHA from 'react-google-recaptcha'
import { ValidationError } from 'ui/common'
import css from './style.scss'

const RECAPTCHA_SITE_KEY = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'

export default Captcha

Captcha.propTypes = {
  error: PropTypes.string,
  onChange: PropTypes.func
}

function Captcha({
  onChange,
  error
}) {
  return (
    <div className={css.captcha}>
      <ReCAPTCHA
        sitekey={RECAPTCHA_SITE_KEY}
        onChange={onChange}
      />
      <div className={css.error}><ValidationError error={error} /></div>
    </div>
  )
}
