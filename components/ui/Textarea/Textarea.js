import React from 'react'
import PropTypes from 'prop-types'
import cx from 'classnames'
import { ValidationError } from 'ui/common'
import css from './Textarea.scss'

export default Textarea

Textarea.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  error: PropTypes.string
}

function Textarea({
  id,
  name,
  value = '',
  placeholder,
  onChange,
  error = ''
}) {
  return (
    <div className={cx(css.textarea, value.length && css.notEmpty)}>
      <div className={css.wrap}>
        <textarea id={id} name={name} onChange={onChange} value={value} />
        <label htmlFor={id}><span>{placeholder}</span></label>
      </div>
      <div className={css.error}>
        <ValidationError error={error} />
      </div>
    </div>
  )
}
