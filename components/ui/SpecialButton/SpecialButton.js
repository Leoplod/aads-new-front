import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import StartImgSrc from './img/start.svg'
import PauseImgSrc from './img/pause.svg'
import SettingsImgSrc from './img/settings.svg'
import TrashImgSrc from './img/trash.svg'
import ShowMoreImgSrc from './img/show-more.svg'
import css from './style.scss'
import { values } from 'ramda'

const buttonTypes = {
  START: 'start',
  PAUSE: 'pause',
  SETTINGS: 'settings',
  TRASH: 'trash',
  SHOW_MORE: 'show-more'
}

SpecialButton.propTypes = {
  onClick: PropTypes.func,
  buttonType: PropTypes.oneOf(values(buttonTypes)).isRequired,
  type: PropTypes.string,
  text: PropTypes.string,
  active: PropTypes.bool

}
export default function SpecialButton({
  type = null,
  buttonType = null,
  onClick,
  text,
  active
}) {
  let className = ''
  let BtnIcon = () => null

  switch (buttonType) {
  case buttonTypes.START:
    className = css.start
    BtnIcon = StartImgSrc
  break;
  case buttonTypes.PAUSE:
    className = css.pause
    BtnIcon = PauseImgSrc
  break;
  case buttonTypes.SETTINGS:
    className = css.pause
    BtnIcon = SettingsImgSrc
  break;
  case buttonTypes.TRASH:
    className = css.trash
    BtnIcon = TrashImgSrc
  break;
  case buttonTypes.SHOW_MORE:
    className = css.showMore
    BtnIcon = ShowMoreImgSrc
  break;
  default:
  }

  return (
    <button
      className={cn(
        className,
        css.specialButton,
        active ? css.active : ''
      )}
      type={type}
      onClick={onClick}
    >
      <BtnIcon className={css.image} />
      {text && (
        <span dangerouslySetInnerHTML={{__html: text}} />
      )}
    </button>
  )
}
