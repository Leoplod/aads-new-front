import React from 'react'
import css from './style.scss'
import { AccountMenu, AccountHeader } from 'account'
import ModalRoot from 'common/ModalRoot/ModalRoot'
import { withRedux } from 'store/with-redux'

function AccountLayout(props) {
  const { children } = props

  return (
    <div className={css.layout}>
      <ModalRoot />
      <div className={css.menuWrap}>
        <AccountMenu />
      </div>
      <div className={css.main}>
        <div className={css.headerWrap}><AccountHeader /></div>
        <div className={css.contentWrap}>
          {children}
        </div>
      </div>
    </div>
  )
}

export default withRedux(AccountLayout)
