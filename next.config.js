const withSass = require('@zeit/next-sass');
const path = require('path');

module.exports = withSass({
  webpack: (config, options) => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty',
    };

    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack']
    });

    config.resolve.modules.push(
      path.resolve('./'),
      path.resolve('./components')
    );

    return config;
  },

  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: '[local]___[hash:base64:5]',
    camelCase: true,
  },
});
