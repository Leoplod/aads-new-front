import React from 'react'
import { compose } from 'ramda'
import { AccountLayout } from 'layouts'
import { AccountInfo } from 'account'
import { withRedux } from 'store/with-redux'

const Info = () => (
  <AccountLayout>
    <AccountInfo />
  </AccountLayout>
)

export default Info
