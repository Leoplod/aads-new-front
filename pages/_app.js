import React from 'react'
import App from 'next/app'
import 'libs/css/body.scss'
import Head from 'next/head'

export default class extends App {
  render() {
    const { Component, pageProps } = this.props
    return (
      <React.Fragment>
        <Head>
          <link rel='icon' href='/favicon.png' />
        </Head>
        <Component {...pageProps} />
      </React.Fragment>
    )
  }
}
