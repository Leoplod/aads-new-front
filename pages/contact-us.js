import React from 'react'
import { ContactUsContainer } from 'forms'

export default ContactUsPage

function ContactUsPage() {
  return (
    <ContactUsContainer />
  )
}
