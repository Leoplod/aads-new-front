import { createStore, applyMiddleware } from 'redux'
import rootReducer from './root-reducer'
import thunkMiddleware from 'redux-thunk';
import { createLogger} from 'redux-logger'

const logger = createLogger({})

export const initStore = (preloadedState = {}) => {
  return createStore(
    rootReducer,
    applyMiddleware(logger)
  )
}
