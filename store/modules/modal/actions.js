import {
  SHOW_MODAL,
  HIDE_MODAL
} from './constants'

export const showModal = (modalType, modalProps) => {
  return {
    type: SHOW_MODAL,
    modalType
  }
}

export const closeModal = () => {
  return {
    type: HIDE_MODAL
  }
}
