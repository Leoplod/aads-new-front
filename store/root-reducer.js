import { combineReducers } from 'redux'
import userReducer from './modules/user/reducer'
import modalReducer from './modules/modal/reducer'

export default combineReducers({
  userState: userReducer,
  modalState: modalReducer
})
